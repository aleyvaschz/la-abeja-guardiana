export default class GalleryServices {
	constructor () {}

	getGalleries() {
		let listOfGalleries = require('./galleries.json')
		return new Promise((resolve) => {
			resolve(listOfGalleries)
		})
	}

	getDetailedGallery(id) {
		let detailedGallery = require('./detailed-gallery')
		return new Promise((resolve) => {
			resolve(detailedGallery.find(gallery => gallery.id === id))
		})
	}
}
