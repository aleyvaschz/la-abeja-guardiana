import Vue from 'vue'
import Router from 'vue-router'
import Landing from '../scenes/Landing/Landing'
import Culture from '../scenes/Culture/Culture'
import Gallery from '../scenes/Gallery/Gallery'
import Library from '../scenes/Library/Library'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
	  {
		  path: '/',
		  name: 'Landing',
		  component: Landing
	  },
	  {
		  path: '/culture',
		  name: 'Culture',
		  component: Culture
	  },
	  {
		  path: '/gallery',
		  name: 'Gallery',
		  component: Gallery
	  },
	  {
		  path: '/library',
		  name: 'Library',
		  component: Library
	  }
  ]
})
